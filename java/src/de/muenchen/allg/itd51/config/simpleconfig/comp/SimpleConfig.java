/*
 * Dateiname: SimpleConfig.java
 * Projekt  : SimpleConfig
 * Funktion : UNO-Service zum Abfragen von Werten aus
 *            ConfigThingy-Konfigurationsdateien
 * 
 * Copyright (c) 2009 Landeshauptstadt München
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the European Union Public Licence (EUPL), 
 * version 1.0.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * European Union Public Licence for more details.
 *
 * You should have received a copy of the European Union Public Licence
 * along with this program. If not, see 
 * http://ec.europa.eu/idabc/en/document/7330
 *
 * Änderungshistorie:
 * Datum      | Wer | Änderungsgrund
 * -------------------------------------------------------------------
 * 27.05.2009 | BED | Erstellung
 * 10.06.2009 | BED | Für Zugriff auf Elemente muss nun kompletter Pfad angegeben werden
 *                  | -> Methoden entsprechend angepasst
 *                  | Konstruktor erzeugt nun auf jeden Fall ConfigThingy
 * 12.06.2009 | BED | Fehler behoben, der auftrat wenn Knotenname in einem Teilbaum mehrmals vorkam
 *                  | Falls Konfigurationsdatei nicht existiert wird Default verwendet
 * 15.06.2009 | BED | Default-Konfigurationsdateien ohne vorangestellten Punkt
 * 19.06.2009 | BED | Benutzt jetzt SimpleConfigSingleton
 * 22.09.2009 | BED | Erweitert um Methoden zum Schreiben von Konfigurationsdaten
 * 28.09.2009 | BED | setElementValue liefert jetzt boolean zurück
 * 14.10.2009 | BNK | +prefix
 * 15.10.2009 | BED | Dokumentation aktualisiert, getElementType geändert
 * -------------------------------------------------------------------
 *
 * @author Daniel Benkmann (D-III-ITD-D101)
 * 
 */
package de.muenchen.allg.itd51.config.simpleconfig.comp;

import com.sun.star.container.NoSuchElementException;
import com.sun.star.lang.IllegalArgumentException;
import com.sun.star.lang.WrappedTargetException;
import com.sun.star.lang.XServiceInfo;
import com.sun.star.lang.XSingleComponentFactory;
import com.sun.star.lib.uno.helper.Factory;
import com.sun.star.lib.uno.helper.WeakBase;
import com.sun.star.registry.XRegistryKey;
import com.sun.star.uno.Type;

import de.muenchen.allg.itd51.config.parser.ConfigThingy;
import de.muenchen.allg.itd51.config.simpleconfig.SimpleConfigSingleton;
import de.muenchen.allg.itd51.config.simpleconfig.XSimpleConfig;

/**
 * Der SimpleConfig-Service erlaubt das Abfragen von Werten aus
 * Konfigurationsdateien, die der Syntax der WollMux-Konfiguration entsprechen
 * (Listen werden allerdings nicht unterstützt).
 * 
 * @author Daniel Benkmann (D-III-ITD-D101)
 */
public class SimpleConfig extends WeakBase implements XServiceInfo, XSimpleConfig
{
  /**
   * Dieses Feld enthält eine Liste aller Services, die dieser UNO-Service
   * implementiert.
   */
  private static final String[] SERVICENAMES =
    { "de.muenchen.allg.itd51.config.simpleconfig.SimpleConfig" };

  /**
   * Suchpfad-Präfix für alle Operationen auf dem Konfigurationsbaum, d.h. bei allen
   * Operationen, die auf path aufgerufen werden, wird automatisch prefix vor path
   * vorangestellt, so dass im Endeffekt nur jeweils in dem Teilbaum gearbeitet wird,
   * der mit prefix anfängt. MUSS IMMER AUF SLASH ENDEN ODER LEERSTRING SEIN ("/" ist
   * nicht zugelassen)!
   */
  private String prefix;

  /**
   * Default-Konstruktor der Klasse, initialisiert {@link #config} mit dem
   * Konfigurationsbaum der unter {@link #CONFIG_PATH} zu findenden
   * Konfigurationsdatei und setzt {@link #prefix} auf Leerstring.
   * 
   * @author Daniel Benkmann (D-III-ITD-D101)
   */
  public SimpleConfig()
  {
    this("");
  }

  /**
   * Konstruktor für SimpleConfig-Objekte, die nur auf einem Teilbaum des
   * Konfigurationsbaums operieren. Initialisiert {@link #config} mit dem
   * Konfigurationsbaum der unter {@link #CONFIG_PATH} zu findenden
   * Konfigurationsdatei und setzt {@link #prefix} auf den übergebenen String-Wert
   * (dieser muss mit Slash enden oder der Leerstring sein; "/" alleine ist nicht
   * zulässig).
   * 
   * @author Daniel Benkmann (D-III-ITD-D101)
   */
  public SimpleConfig(String prefix)
  {
    SimpleConfigSingleton.initialize();
    this.prefix = prefix;
  }

  /**
   * Diese Methode liefert eine Factory zurück, die in der Lage ist den UNO-Service
   * zu erzeugen. Die Methode wird von UNO intern benötigt. Die Methoden
   * {@link #__getComponentFactory(String)} und
   * {@link #__writeRegistryServiceInfo(XRegistryKey)} stellen das Herzstück des
   * UNO-Service dar.
   * 
   * @param sImplName
   *          Name der Service-Implementierung
   * @return eine Factory zum Erzeugen des UNO-Services
   */
  public synchronized static XSingleComponentFactory __getComponentFactory(
      String sImplName)
  {
    XSingleComponentFactory xFactory = null;
    if (sImplName.equals(SimpleConfig.class.getName()))
    {
      xFactory = Factory.createComponentFactory(SimpleConfig.class, SERVICENAMES);
    }
    return xFactory;
  }

  /**
   * Diese Methode registriert den UNO-Service. Sie wird z.B. beim unopkg-add im
   * Hintergrund aufgerufen. Die Methoden {@link #__getComponentFactory(String)} und
   * {@link #__writeRegistryServiceInfo(XRegistryKey)} stellen das Herzstück des
   * UNO-Service dar.
   * 
   * @param xRegKey
   * @return
   */
  public synchronized static boolean __writeRegistryServiceInfo(XRegistryKey xRegKey)
  {
    return Factory.writeRegistryServiceInfo(SimpleConfig.class.getName(),
      SimpleConfig.SERVICENAMES, xRegKey);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.sun.star.lang.XServiceInfo#getImplementationName()
   */
  public String getImplementationName()
  {
    return SimpleConfig.class.getName();
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.sun.star.lang.XServiceInfo#getSupportedServiceNames()
   */
  public String[] getSupportedServiceNames()
  {
    return SERVICENAMES;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.sun.star.lang.XServiceInfo#supportsService(java.lang.String)
   */
  public boolean supportsService(String sService)
  {
    int len = SERVICENAMES.length;
    for (int i = 0; i < len; i++)
    {
      if (sService.equals(SERVICENAMES[i])) return true;
    }
    return false;
  }

  /**
   * Führt im Konfigurationsbaum der eingelesenen Konfigurationsdatei eine Suche nach
   * Knoten durch, deren Pfad im Baum dem übergebenen name (mit vorangestelltem
   * {@link #prefix}) entspricht, und liefert entweder den String-Wert des Blattes
   * direkt unterhalb des gefundenen Knotens zurück, oder, falls dort kein Blatt
   * existiert, ein neues SimpleConfig-Objekt mit dem Suchpfad als Präfix. Der Pfad
   * ("name") des gesuchten Knotens muss in der Syntax "Knoten1/Knoten2/.../KnotenN"
   * angegeben werden, wobei "KnotenN" der Name des gesuchten Knotens und "Knoten1"
   * ein direkter Kindknoten der Baumwurzel ist (d.h. die Baumwurzel des intern
   * verwendeten {@link ConfigThingy} selbst wird im Pfad nicht mitangegeben, da ihr
   * Name auch nicht in der Konfigurationsdatei auftaucht und beliebig ist).
   * 
   * @param name
   *          der Pfad im Baum zum gesuchten Knoten
   * @return den Wert des Blattes direkt unterhalb des gesuchten Knotens oder ein
   *         neues SimpleConfig-Objekt mit dem Suchpfad als Präfix, wenn der
   *         gefundene Knoten nicht der direkte Vaterknoten eines Blattes ist.
   *         Sollten im Baum mehrere Knoten unter demselben Pfad existieren, so wird
   *         nur der in der Konfigurationsdatei zuletzt definierte Knoten betrachtet.
   * @throws NoSuchElementException
   *           wenn kein Knoten unter dem übergebenen Pfad gefunden wurde oder der
   *           Pfad leer ist
   * @author Daniel Benkmann (D-III-ITD-D101)
   * 
   * @see com.sun.star.container.XNameAccess#getByName(java.lang.String)
   */
  public Object getByName(String name) throws NoSuchElementException,
      WrappedTargetException
  {
    return SimpleConfigSingleton.getInstance().getLeafOrSubtreeByPath(prefix + name);
  }

  /**
   * Liefert die Pfade zu allen Blättern des Konfigurationsbaums (bzw. Teilbaums,
   * falls {@link #prefix} nicht leer ist) der Konfigurationsdatei zurück - in der
   * Syntax "Knoten1/Knoten2/.../KnotenN", wobei "KnotenN" der Name des direkten
   * Vaterknotens des jeweiligen Blattes ist. Dies sind alle legalen Suchpfade, die
   * als Parameter <code>name</code> der Methode {@link #getByName(String)} einen
   * String-Wert zurückliefern.
   * 
   * Die Reihenfolge der zurückgelieferten Pfade ist nicht genauer spezifiziert.
   * Sollte in der Konfigurationdatei ein Pfad mehrmals verwendet worden sein, so ist
   * er in dem von dieser Methode zurückgelieferten Ergebnis dennoch nur einmal
   * enthalten.
   * 
   * @return ein String-Array, das alle gültigen Pfade zu Blättern im
   *         Konfigurations(teil)baum enthält. Die Reihenfolge im Array ist nicht
   *         genauer spezifiziert. Das Array ist frei von Dubletten.
   * @author Daniel Benkmann (D-III-ITD-D101)
   * 
   * @see com.sun.star.container.XNameAccess#getElementNames()
   */
  public String[] getElementNames()
  {
    return SimpleConfigSingleton.getInstance().getPathsToLeaves(prefix);
  }

  /**
   * Liefert <code>true</code> zurück, wenn ein Aufruf von
   * {@link #getByName(String)} mit dem übergebenen Parameter <code>name</code> zu
   * einem Ergebnis führen würde, <code>false</code> sonst.
   * 
   * @param name
   *          der Pfad des gesuchten Blattes (siehe Erläuterungen unter
   *          {@link #getByName(String)})
   * @author Daniel Benkmann (D-III-ITD-D101)
   * 
   * @see com.sun.star.container.XNameAccess#hasByName(java.lang.String)
   */
  public boolean hasByName(String name)
  {
    try
    {
      getByName(prefix + name);
    }
    catch (Exception e)
    {
      return false;
    }
    return true;
  }

  /**
   * Liefert {@link Type#VOID} zurück, da es sich gewissermaßen um einen "multi-type
   * container" handelt und der genaue Typ der Rückgabewerte nicht bestimmt werden
   * kann (ist entweder String oder SimpleConfig). Die Methode dürfte in der Praxis
   * ohnehin nicht verwendet werden und existiert nur, weil das Interface es
   * verlangt.
   * 
   * @author Daniel Benkmann (D-III-ITD-D101)
   * 
   * @see com.sun.star.container.XElementAccess#getElementType()
   */
  public Type getElementType()
  {
    return Type.VOID;
  }

  /**
   * Liefert <code>true</code> zurück, wenn der Konfigurationsbaum der
   * Konfigurationsdatei irgendwelche Elemente/Knoten enthält, <code>false</code>
   * sonst.
   * 
   * @author Daniel Benkmann (D-III-ITD-D101)
   * 
   * @see com.sun.star.container.XElementAccess#hasElements()
   */
  public boolean hasElements()
  {
    return SimpleConfigSingleton.getInstance().hasLeaves();
  }

  /**
   * Setzt den Wert des Blattes direkt unterhalb des Knotens, der sich unter dem
   * übergebenen Pfad "name" im Konfigurationsbaum (bzw. Teilbaum, wenn
   * {@link #prefix} nicht leer ist) befindet, auf den übergebenen Wert "value". Wenn
   * noch kein Blatt unter dem übergebenen Pfad existiert, so wird eines angelegt.
   * 
   * @param name
   *          der Pfad im Konfigurationsbaum zum Blatt, dessen Wert geändert werden
   *          soll (siehe auch Erläuterungen zum Pfad unter
   *          {@link #getByName(String)})
   * @param value
   *          der Wert, den das Blatt erhalten soll
   * @return <code>true</code>, wenn das Setzen des Wertes erfolgreich war,
   *         <code>false</code> sonst
   * @throws IllegalArgumentException
   *           wenn name == <code>null</code> ist oder Länge 0 hat, oder wenn value ==
   *           <code>null</code> ist; außerdem wenn einer der Schlüsselnamen im
   *           Pfad unerlaubte Zeichen enthält (erlaubte Zeichen sind die Buchstaben
   *           a-z und A-Z, sowie Ziffern und der Unterstrich, wobei das erste
   *           Zeichen keine Ziffer sein darf)
   * @author Daniel Benkmann (D-III-ITD-D101)
   * 
   * @see de.muenchen.allg.itd51.config.simpleconfig.XSimpleConfig#setElementValue(java.lang.String,
   *      java.lang.String)
   */
  public boolean setElementValue(String name, String value)
      throws IllegalArgumentException
  {
    return SimpleConfigSingleton.getInstance().setLeaf(prefix + name, value);
  }

  /**
   * Entfernt den Knoten, der sich unter dem Pfad "name" im Konfigurationsbaum (bzw.
   * Teilbaum, wenn {@link #prefix} nicht leer ist) befindet, sowie alle Knoten, die
   * unter ihm liegen. Dies funktioniert nur mit Knoten, die in einer schreibbaren
   * Konfigurationsdatei festgelegt sind. Wenn mehrere Knoten mit demselben Pfad
   * existieren, wird nur der zuletzt in der schreibbaren Konfigurationsdatei
   * definierte entfernt.
   * 
   * @param name
   *          der Pfad im Konfigurations(teil)baum zum Knoten, der entfernt werden
   *          soll (siehe auch Erläuterungen zum Pfad unter
   *          {@link #getByName(String)})
   * @return <code>true</code> wenn das Entfernen erfolgreich war,
   *         <code>false</code> sonst
   * 
   * @author Daniel Benkmann (D-III-ITD-D101)
   * @see de.muenchen.allg.itd51.config.simpleconfig.XSimpleConfig#removeElement(java.lang.String)
   */
  public boolean removeElement(String name)
  {
    return SimpleConfigSingleton.getInstance().removeNode(prefix + name);
  }

}
