/*
 * Dateiname: SimpleConfigSingleton.java
 * Projekt  : SimpleConfig
 * Funktion : Singleton des SimpleConfig-Service
 * 
 * Copyright (c) 2009 Landeshauptstadt München
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the European Union Public Licence (EUPL), 
 * version 1.0.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * European Union Public Licence for more details.
 *
 * You should have received a copy of the European Union Public Licence
 * along with this program. If not, see 
 * http://ec.europa.eu/idabc/en/document/7330
 *
 * Änderungshistorie:
 * Datum      | Wer | Änderungsgrund
 * -------------------------------------------------------------------
 * 19.06.2009 | BED | Erstellung
 * 15.09.2009 | BED | + Auswertung von .makro_test.conf
 * 22.09.2009 | BED | + schreibbare .makro_user.conf
 * 28.09.2009 | BED | Änderung an Fehlerbehandlung -> setLeaf liefert jetzt boolean zurück
 * 14.10.2009 | BNK | +prefix und sich daraus ergebende Änderungen
 * 15.10.2009 | BED | Dokumentation aktualisiert, kleinere Fixes und Aufräumen
 * 02.12.2009 | BED | Auch Lese-Methoden synchronized gemacht
 * -------------------------------------------------------------------
 *
 * @author Daniel Benkmann (D-III-ITD-D101)
 * 
 */
package de.muenchen.allg.itd51.config.simpleconfig;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sun.star.container.NoSuchElementException;
import com.sun.star.lang.IllegalArgumentException;

import de.muenchen.allg.itd51.config.parser.ConfigThingy;
import de.muenchen.allg.itd51.config.parser.NodeNotFoundException;
import de.muenchen.allg.itd51.config.simpleconfig.comp.SimpleConfig;

/**
 * Diese Klasse ist ein Singleton, welches die Daten der eingelesenen
 * SimpleConfig-Konfigurationsdatei hält und die Implementierung der zentralen
 * Methoden des SimpleConfig-Services zur Verfügung stellt.
 * 
 * @author Daniel Benkmann (D-III-ITD-D101)
 */
public class SimpleConfigSingleton
{

  /** Die Singleton-Instanz */
  private static SimpleConfigSingleton singletonInstance = null;

  /**
   * Regulärer Ausdruck zur Identifikation von legalen Schlüsseln.
   */
  private static Pattern keyPattern = Pattern.compile("^([a-zA-Z_][a-zA-Z_0-9]*)");

  /**
   * Der Pfad zur einzulesenden Konfigurationsdatei. Die Konfigurationsdatei muss der
   * WollMux-Syntax entsprechen.
   */
  private static final String CONFIG_PATH =
    System.getProperty("user.home") + "/.makro.conf";

  /**
   * Pfad zur Default-Konfigurationsdatei unter Linux.
   */
  private static final String DEFAULT_CONFIG_PATH_LINUX =
    "/etc/simpleconfig/makro.conf";

  /**
   * Pfad zur Default-Konfigurationsdatei unter Windows.
   */
  private static final String DEFAULT_CONFIG_PATH_WINDOWS =
    "C:\\Programme\\simpleconfig\\makro.conf";

  /**
   * Pfad zur Test-Konfigurationsdatei. Diese Konfigurationsdatei wird, falls
   * vorhanden, immer an den Schluss des erzeugten ConfigThingys inkludiert (aber vor
   * der schreibbaren Konfigurationsdatei, siehe {@link #WRITABLE_CONFIG_PATH}!) und
   * dient dazu auf einfache Weise Tests durchführen zu können ohne die eigentliche
   * Konfigurationdatei verändern zu müssen.
   */
  private static final String TEST_CONFIG_PATH =
    System.getProperty("user.home") + "/.makro_test.conf";

  /**
   * Pfad zur schreibbaren Konfigurationsdatei, in die Änderungen mittels
   * {@link #setLeaf(String, String)} geschrieben werden. Diese Konfigurationsdatei
   * wird, falls vorhanden, immer an den Schluss des erzeugten ConfigThingys
   * inkludiert (noch nach der Test-Konfigurationsdatei, siehe
   * {@link #TEST_CONFIG_PATH}!).
   */
  private static final String WRITABLE_CONFIG_PATH =
    System.getProperty("user.home") + "/.makro_user.conf";

  /**
   * Enthält den geparsten Konfigurationsbaum der eingelesenen Konfigurationsdatei
   * (inklusive Test-Konfigurationsdatei und schreibbarer Konfigurationsdatei).
   */
  private ConfigThingy config;

  /**
   * Enthält den geparsten Konfigurationsbaum der eingelesenen Konfigurationsdatei
   * (inklusive Test-Konfigurationsdatei, aber OHNE die schreibbare
   * Konfigurationsdatei). Dieses ConfigThingy wird beim Initialisieren des
   * SimpleConfigSingletons erschaffen und danach nicht mehr verändert. Sollten
   * Änderungen an der schreibbaren Konfigurationsdatei stattfinden, muss nur diese
   * neu geparst werden und an configWithoutWritable angehängt werden, um
   * {@link #config} zu aktualisieren.
   */
  private final ConfigThingy configWithoutWritable;

  /**
   * Enthält den geparsten Konfigurationsbaum der eingelesenen (optionalen)
   * schreibbaren Konfigurationsdatei, die sich unter {@link #WRITABLE_CONFIG_PATH}
   * befindet.
   */
  private ConfigThingy writableConfig;

  /**
   * Konstruktor der Klasse (private da Singleton), initialisiert {@link #config} mit
   * dem Konfigurationsbaum der unter {@link #CONFIG_PATH} zu findenden
   * Konfigurationsdatei.
   * 
   * @author Daniel Benkmann (D-III-ITD-D101)
   */
  private SimpleConfigSingleton()
  {
    File confFile = new File(CONFIG_PATH);

    // Wenn die Konfigurationsdatei nicht existiert, verwenden wir eine
    // Default-Konfigurationsdatei
    if (!confFile.exists())
    {
      File[] roots = File.listRoots(); // zum Testen ob Windows oder Linux
      String defaultConfPath = DEFAULT_CONFIG_PATH_LINUX;
      if (roots.length > 0 && roots[0].toString().contains(":"))
      {
        defaultConfPath = DEFAULT_CONFIG_PATH_WINDOWS;
      }
      confFile = new File(defaultConfPath);
    }

    try
    {
      this.config = new ConfigThingy("config", confFile.toURI().toURL());
    }
    catch (Exception e)
    {
      // Falls beim Erzeugen des ConfigThingy eine Exception auftritt, so erzeugen
      // wir zu Debug-Zwecken ein neues ConfigThingy, das Informationen über die
      // aufgetretene Exception enthält. Über getElementNames() kann so festgestellt
      // werden, ob und welche Exception aufgetreten ist.
      this.config = new ConfigThingy("config");
      ConfigThingy exceptionName = this.config.add(e.getClass().getSimpleName());
      exceptionName.add(e.getMessage());
    }

    File testConfFile = new File(TEST_CONFIG_PATH);
    // Falls die Test-Konfigurationsdatei existiert, inkludieren wir sie am Schluss
    if (testConfFile.exists())
    {
      ConfigThingy testConfigThingy = new ConfigThingy("testConfig");
      try
      {
        testConfigThingy =
          new ConfigThingy("testConfig", testConfFile.toURI().toURL());

      }
      catch (Exception e)
      {
        // Falls beim Erzeugen des ConfigThingy eine Exception auftritt, so erzeugen
        // wir zu Debug-Zwecken ein neues ConfigThingy, das Informationen über die
        // aufgetretene Exception enthält. Über getElementNames() kann so
        // festgestellt werden, ob und welche Exception aufgetreten ist.
        ConfigThingy exceptionName =
          testConfigThingy.add(e.getClass().getSimpleName());
        exceptionName.add(e.getMessage());
      }
      for (ConfigThingy child : testConfigThingy)
      {
        this.config.addChild(child);
      }
    }

    // Kopie des bisher erstellten ConfigThingys speichern
    // zur späteren Wiederverwendung, wenn sich die schreibbare Config ändert
    this.configWithoutWritable = new ConfigThingy(config);

    this.writableConfig = new ConfigThingy("writableConfig");
    File writableConfFile = new File(WRITABLE_CONFIG_PATH);
    // Falls die schreibbare Konfigurationsdatei existiert
    if (writableConfFile.exists())
    {
      try
      {
        this.writableConfig =
          new ConfigThingy("writableConfig", writableConfFile.toURI().toURL());
      }
      catch (Exception e)
      {
        // Falls beim Erzeugen des ConfigThingy eine Exception auftritt, so erzeugen
        // wir zu Debug-Zwecken ein neues ConfigThingy, das Informationen über die
        // aufgetretene Exception enthält. Über getElementNames() kann so
        // festgestellt werden, ob und welche Exception aufgetreten ist.
        ConfigThingy exceptionName =
          this.writableConfig.add(e.getClass().getSimpleName());
        exceptionName.add(e.getMessage());
      }
      for (ConfigThingy child : this.writableConfig)
      {
        this.config.addChild(child);
      }
    }
  }

  /**
   * Diese Methode liefert die Instanz des SimpleConfigSingletons. Ist das Singleton
   * noch nicht initialisiert, so liefert die Methode null!
   * 
   * @return Instanz des SimpleConfigSingletons oder null.
   */
  public static SimpleConfigSingleton getInstance()
  {
    return singletonInstance;
  }

  /**
   * Diese Methode initialisiert das SimpleConfigSingleton (nur dann, wenn es noch
   * nicht initialisiert wurde)
   */
  public static synchronized void initialize()
  {
    if (singletonInstance == null)
    {
      singletonInstance = new SimpleConfigSingleton();
    }
  }

  /**
   * Führt im Konfigurationsbaum der eingelesenen Konfigurationsdatei eine Suche nach
   * Knoten durch, deren Pfad im Baum dem übergebenen path entspricht, und liefert
   * entweder den (String-)Wert des einzigen Blattes zurück, das direkt unter dem
   * gefundenen Knoten liegt, oder falls so ein Blatt nicht existiert ein neues
   * {@link SimpleConfig}-Objekt mit path als Präfix, das den Teilbaum unter dem
   * gefundenen Knoten repräsentiert. Wenn der gefundene Knoten selbst ein Blatt ist,
   * wird eine {@link NoSuchElementException} geworfen, ebenso wenn kein Knoten
   * gefunden wurde.
   * 
   * Der Pfad des gesuchten Knotens muss in der Syntax "Knoten1/Knoten2/.../KnotenN"
   * angegeben werden, wobei "KnotenN" der Name des gesuchten Knotens ist und
   * "Knoten1" ein direkter Kindknoten der Baumwurzel (d.h. die Baumwurzel von
   * {@link #config} selbst wird im Pfad nicht mitangegeben, da ihr Name auch nicht
   * in der Konfigurationsdatei, aus der der Baum erzeugt wird, auftaucht und
   * beliebig ist).
   * 
   * @param path
   *          der Pfad im Baum zum gesuchten Knoten
   * @return den String-Wert des Blattes, das direkt unter dem gesuchten Knoten hängt
   *         oder ein neues SimpleConfig-Objekt, das den Teilbaum unter dem
   *         gefundenen Knoten repräsentiert, falls dort kein Blatt liegt. Sollten im
   *         Baum mehrere Knoten unter angegebenen Pfad existieren, so wird nur der
   *         in der Konfigurationsdatei zuletzt definierte Knoten betrachtet.
   * @throws NoSuchElementException
   *           wenn der gefundene Knoten selbst ein Blatt ist, kein Knoten unter dem
   *           übergebenen Pfad gefunden wurde oder der Pfad leer (oder
   *           <code>null</code>) ist
   * @author Daniel Benkmann (D-III-ITD-D101)
   */
  public synchronized Object getLeafOrSubtreeByPath(String path)
      throws NoSuchElementException
  {
    ConfigThingy conf = getConfigThingyByPath(path, this.config);
    if (isLeafParent(conf))
    {
      return conf.toString();
    }
    else
    {
      if (!path.endsWith("/")) path = path + "/";
      return new SimpleConfig(path);
    }
  }

  /**
   * Führt im Konfigurationsbaum des übergebenen ConfigThingy eine Suche nach Knoten
   * durch, deren Pfad im Baum dem übergebenen path entspricht und die mindestens ein
   * Kind haben, und liefert eine Referenz auf das entsprechende ConfigThingy zurück.
   * Der Pfad des gesuchten Knotens muss in der Syntax "Knoten1/Knoten2/.../KnotenN"
   * angegeben werden, wobei "KnotenN" der Name des gesuchten Knotens selbst ist und
   * "Knoten1" ein direkter Kindknoten der Baumwurzel ist (d.h. die Baumwurzel des
   * ConfigThingy wird im Pfad nicht mitangegeben, da ihr Name auch nicht in der
   * Konfigurationsdatei, aus der das ConfigThingy erstellt wurde, auftaucht und
   * beliebig ist).
   * 
   * @param path
   *          der Pfad im Baum zum gesuchten Knoten
   * @param cf
   *          ConfigThingy, das durchsucht werden soll
   * @return eine Referenz auf das ConfigThingy, das den gesuchten Knoten
   *         repräsentiert. Sollten im Baum mehrere Knoten unter demselben Pfad
   *         existieren, so wird nur der in der Konfigurationsdatei, aus der das
   *         ConfigThingy erstellt wurde, zuletzt definierte Knoten (bzw. genauer
   *         gesagt eine Referenz auf das ConfigThingy, das diesen Wert
   *         repräsentiert) zurückgeliefert.
   * @throws NoSuchElementException
   *           wenn kein Knoten unter dem übergebenen Pfad gefunden wurde oder der
   *           Pfad leer (oder <code>null</code>) ist oder cf <code>null</code>
   *           ist
   * @author Daniel Benkmann (D-III-ITD-D101)
   */
  private static ConfigThingy getConfigThingyByPath(String path, ConfigThingy cf)
      throws NoSuchElementException
  {
    if (path == null || path.length() == 0)
    {
      throw new NoSuchElementException("Leerer Elementname nicht erlaubt");
    }
    if (cf == null)
    {
      throw new NoSuchElementException("Leere Konfigurationsdatei");
    }

    Scanner scanner = new Scanner(path);
    scanner.useDelimiter("/");
    String currentName = scanner.next();

    ConfigThingy found = cf.query(currentName, 1);

    while (scanner.hasNext())
    {
      // Falls found keine Kinder hat, müssen wir nicht weitersuchen
      if (found.count() == 0) break;
      currentName = scanner.next();
      found = found.query(currentName, 2, 2);
    }

    // Knoten in der Ergebnisliste, die nicht mindestens ein Kind besitzen, werden
    // aus der Ergebnisliste entfernt.
    Iterator<ConfigThingy> iter = found.iterator();
    while (iter.hasNext())
    {
      ConfigThingy c = iter.next();
      if (c.count() == 0) iter.remove();
    }

    if (found.count() == 0) // kein Knoten mit name gefunden
    {
      throw new NoSuchElementException("Element mit Name '" + path
        + "' nicht in Konfigurationsdatei gefunden");
    }

    try
    {
      // falls mehrere mögliche Ergebnisse gefunden wurden, nehmen wir das letzte
      found = found.getLastChild();
    }
    catch (NodeNotFoundException e)
    {
      // kann nicht passieren, da wir oben schon überprüft haben, ob count() == 0 und
      // daher getLastChild() immer ein Ergebnis zurückliefert, d.h. found wird
      // nie null bleiben
    }
    return found;
  }

  /**
   * Liefert die Pfade zu allen Blättern des Konfigurationsbaums der
   * Konfigurationsdatei zurück - in der Syntax "Knoten1/Knoten2/.../KnotenN", wobei
   * "KnotenN" der Name des direkten Vaterknotens des jeweiligen Blattes ist und
   * "Knoten1" ein direkter Kindknoten der Baumwurzel (d.h. die Baumwurzel von
   * {@link #config} selbst ist im Pfad nicht mitangegeben, da ihr Name auch nicht in
   * der Konfigurationsdatei auftaucht und beliebig ist). Dies sind alle legalen
   * Suchpfade, die als Parameter <code>path</code> der Methode
   * {@link #getLeafOrSubtreeByPath(String)} übergeben als Ergebnis einen String-Wert
   * zurückliefern.
   * 
   * Über den Parameter prefix können die Suchergebnisse weiter eingeschränkt werden.
   * Es werden nur die Blätterpfade zurückgeliefert, die mit dem übergebenen Präfix
   * anfangen, wobei das Präfix selbst im Rückgabe-String dann nicht mehr auftaucht.
   * Soll keine Einschränkung gemacht werden, kann der Leerstring übergeben werden.
   * 
   * Die Reihenfolge der zurückgelieferten Pfade ist nicht genauer spezifiziert.
   * Sollte in der Konfigurationdatei ein Pfad mehrmals verwendet worden sein, so ist
   * er in dem von dieser Methode zurückgelieferten Ergebnis dennoch nur einmal
   * enthalten.
   * 
   * @param prefix
   *          Pfad-Präfix, das alle Suchergebnisse haben müssen
   * @return ein String-Array, das alle gültigen Pfade zu Blättern im
   *         Konfigurationsbaum enthält. Die Reihenfolge im Array ist nicht genauer
   *         spezifiziert. Das Array ist frei von Dubletten.
   * @author Daniel Benkmann (D-III-ITD-D101)
   */
  public synchronized String[] getPathsToLeaves(String prefix)
  {
    Set<String> elementNamesList = new HashSet<String>();
    for (ConfigThingy conf : this.config)
    {
      collectPathsToLeaves("", conf, elementNamesList, prefix);
    }
    String[] elementNames = elementNamesList.toArray(new String[0]);
    return elementNames;
  }

  /**
   * Hilfsmethode zum Aufsammeln aller Pfade zu Blättern in einem Konfigurationsbaum.
   * Die Pfade werden mit Slashes getrennt in der Form "Knoten1/Knoten2/.../KnotenN"
   * in der Liste pathList gespeichert, wobei "KnotenN" der direkte Elternknoten
   * eines Blatts ist. Über den Parameter prefix können die gefundenen Pfade weiter
   * eingeschränkt werden. Der übergebene Pfad-Präfix selbst taucht in den
   * zurückgelieferten Pfaden dann nicht mehr auf.
   * 
   * Diese Methode wird von {@link #getPathsToLeaves()} benötigt.
   * 
   * @param confPath
   *          der Pfad im Baum zum Knoten conf
   * @param conf
   *          ein Knoten im Baum
   * @param collectedPaths
   *          die Collection, in der alle Pfade zu Blättern gesammelt werden
   * @param prefix
   *          Präfix, mit dem alle gefundenen Pfade anfangen müssen. Soll keine
   *          Einschränkung gemacht werden, so kann der Leerstring übergeben werden.
   * @author Daniel Benkmann (D-III-ITD-D101)
   */
  private static void collectPathsToLeaves(String confPath, ConfigThingy conf,
      Collection<String> collectedPaths, String prefix)
  {
    if (conf.count() == 0) // Vater eines Blatts?
    {
      if (confPath.startsWith(prefix))
      {
        collectedPaths.add(confPath.substring(prefix.length())); // Präfix weg
      }
    }
    else
    {
      confPath =
        confPath.equals("") ? conf.getName() : confPath + "/" + conf.getName();
      for (ConfigThingy c : conf)
      {
        collectPathsToLeaves(confPath, c, collectedPaths, prefix);
      }
    }
  }

  /**
   * Liefert <code>true</code> zurück, wenn der Konfigurationsbaum der
   * Konfigurationsdatei irgendwelche Blätter enthält, <code>false</code> sonst.
   * 
   * @author Daniel Benkmann (D-III-ITD-D101)
   */
  public synchronized boolean hasLeaves()
  {
    return config.count() > 0;
  }

  /**
   * Setzt den Wert des Blattes direkt unterhalb des Knotens, der sich unter dem
   * übergebenen Pfad "path" im Konfigurationsbaum befindet, auf den übergebenen Wert
   * "value". Wenn zum übergebenen Pfad noch kein Knoten existiert, so werden Knoten
   * und Blatt angelegt. Sollte unter dem Pfad bereits ein Knoten existieren, der
   * allerdings nicht der Vater eines Blatts ist, so wird ein Fehler geworfen.
   * 
   * @param path
   *          der Pfad im Konfigurationsbaum zum Blatt, dessen Wert geändert werden
   *          soll (siehe auch Erläuterungen zum Pfad unter
   *          {@link #getLeafOrSubtreeByPath(String)})
   * @param value
   *          der Wert, den das Blatt erhalten soll
   * @return <code>true</code> wenn der Wert des Blattes erfolgreich geändert
   *         wurde, <code>false</code> sonst
   * @throws IllegalArgumentException
   *           wenn name == <code>null</code> ist oder Länge 0 hat, oder wenn value ==
   *           <code>null</code> ist; außerdem wenn einer der Schlüsselnamen im
   *           Pfad unerlaubte Zeichen enthält (erlaubte Zeichen sind die Buchstaben
   *           a-z und A-Z, sowie Ziffern und der Unterstrich, wobei das erste
   *           Zeichen keine Ziffer sein darf); oder wenn unter dem angegebenen Pfad
   *           ein Knoten existiert, der nicht der Vater eines Blatts ist
   * @author Daniel Benkmann (D-III-ITD-D101)
   */
  public synchronized boolean setLeaf(String path, String value)
      throws IllegalArgumentException
  {
    if (path == null || path.length() == 0 || value == null)
    {
      throw new IllegalArgumentException(
        "Übergebener Elementpfad oder -wert ist Null oder leer!");
    }

    // Wir arbeiten mit einer Kopie um einen einfachen Rollback im Fehlerfall zu
    // ermöglichen
    ConfigThingy writableConfigCopy = new ConfigThingy(this.writableConfig);

    ConfigThingy conf = null;
    try
    {
      conf = getConfigThingyByPath(path, writableConfigCopy);
      if (!isLeafParent(conf))
        throw new IllegalArgumentException("Unter Elementpfad \"" + path
          + "\" liegt ein Knoten, der nicht Vater eines Blatts ist!");
      conf.iterator().next().setName(value);
    }
    catch (NoSuchElementException e)
    {
      // Noch kein entsprechendes Blatt vorhanden => eines anlegen.
      conf = new ConfigThingy(value);

      // Blatt unter dem richtigen Pfad einhängen (wir überprüfen nicht, ob Teile des
      // Pfades eventuell schon vorhanden sind, sondern legen den Pfad einfach
      // komplett neu an. Es ist kein Problem, wenn Teilpfade doppelt existieren.)
      Scanner scanner = new Scanner(path);
      scanner.useDelimiter("/");
      String keyName = scanner.next();
      Matcher m = keyPattern.matcher(keyName);
      if (!m.matches())
      {
        throw new IllegalArgumentException("Illegaler Schlüsselname: \"" + keyName
          + "\"!");
      }

      ConfigThingy root = new ConfigThingy(keyName);
      ConfigThingy parent = root;

      while (scanner.hasNext())
      {
        keyName = scanner.next();
        m = keyPattern.matcher(keyName);
        if (!m.matches())
        {
          throw new IllegalArgumentException("Illegaler Schlüsselname: \"" + keyName
            + "\"!");
        }
        parent = parent.add(keyName);
      }
      // Jetzt noch das Blatt selbst hinzufügen
      parent.addChild(conf);
      // und das ganze zu writableConfig hinzufügen
      writableConfigCopy.addChild(root);
    }

    try
    {
      // Updaten von this.config, this.writableConfig und schreiben der Änderungen in
      // schreibbare Konfigurationsdatei
      updateWritableConfig(writableConfigCopy);
    }
    catch (FileNotFoundException e)
    {
      return false;
    }
    return true;
  }

  /**
   * Liefer true gdw conf genau 1 Kind hat, das selbst keine Kinder hat.
   * 
   * @author Matthias Benkmann (D-III-ITD-D101)
   */
  private boolean isLeafParent(ConfigThingy conf)
  {
    return conf.count() == 1 && conf.iterator().next().count() == 0;
  }

  /**
   * Entfernt den Knoten (sowie alle Unterknoten), der sich unter dem Pfad "path" im
   * Konfigurationsbaum befindet. Dies funktioniert nur mit Knoten, die in der
   * schreibbaren Konfigurationsdatei definiert sind. Wenn mehrere Elemente mit
   * demselben Pfad existieren, wird nur das zuletzt in der schreibbaren
   * Konfigurationsdatei definierte entfernt.
   * 
   * @param path
   *          der Pfad im Konfigurationsbaum zum Knoten, der entfernt werden soll
   *          (siehe auch Erläuterungen zum Pfad unter
   *          {@link #getLeafOrSubtreeByPath(String)})
   * @return <code>true</code> wenn der Knoten erfolgreich entfernt wurde,
   *         <code>false</code> sonst (automatisch der Fall, wenn der Pfad
   *         <code>null</code> ist oder Länge 0 hat)
   * @author Daniel Benkmann (D-III-ITD-D101)
   */
  public synchronized boolean removeNode(String path)
  {
    if (path == null || path.length() == 0)
    {
      return false;
    }

    // Wir arbeiten mit einer Kopie um einen einfachen Rollback im Fehlerfall zu
    // ermöglichen
    ConfigThingy writableConfigCopy = new ConfigThingy(this.writableConfig);
    boolean removalSuccessful = false;
    try
    {
      ConfigThingy conf = getConfigThingyByPath(path, writableConfigCopy);
      removalSuccessful = removeConfigThingy(conf, writableConfigCopy);
      if (removalSuccessful)
      {
        try
        {
          // Updaten von this.config, this.writableConfig und Schreiben der
          // schreibbaren Konfigurationsdatei
          updateWritableConfig(writableConfigCopy);
        }
        catch (FileNotFoundException e)
        {
          return false;
        }
      }
      return removalSuccessful;
    }
    catch (NoSuchElementException e)
    {
      return false;
    }
  }

  /**
   * Hilfsmethode zum Entfernen eines ConfigThingy aus einem anderen ConfigThingy.
   * Die Methode geht rekursiv den kompletten Baum des from-ConfigThingys durch,
   * vergleicht alle Knoten mit dem toRemove-ConfigThingy und entfernt dieses bei
   * Gleichheit (kein equals-Vergleich, sondern Vergleich auf Referenzgleichheit!).
   * Sobald toRemove gefunden und entfernt wurde, wird nicht weiter gesucht sondern
   * <code>true</code> zurückgeliefert, d.h. wenn sich das toRemove-ConfigThingy
   * mehrmals im from-ConfigThingy befindet, wird es nur einmal gelöscht! In dem
   * konkreten Fall, für den diese Hilfsmethode verwendet wird, können wir aber davon
   * ausgehen, dass es keine doppelte Vorkommen gibt, daher ist diese Optimierung in
   * Ordnung.
   * 
   * Durch das Entfernen eines Blattes wird theoretisch der Vaterknoten des
   * entfernten Blattes zum neuen Blatt (sofern das Blatt der einzige Kindknoten
   * seines Vaters war). Dies ist nicht erwünscht, daher löscht die Methode neben dem
   * Knoten auch gleich noch den kompletten "toten Ast", d.h. alle Knoten auf dem
   * Pfad zurück vom Blatt zur Wurzel bis zur ersten Verzweigung.
   * 
   * Aufgrund ihrer Optimierungen und ihres doch speziellen Verhaltens ist diese
   * Methode in ihrer jetztigen Form wahrscheinlich nicht als generelle "Lösche
   * ConfigThingy aus ConfigThingy"-Methode brauchbar.
   * 
   * @param toRemove
   *          ConfigThingy, das entfernt werden soll
   * @param from
   *          ConfigThingy, aus dem entfernt werden soll
   * @return <code>true</code> wenn toRemove entfernt wurde, <code>false</code>
   *         sonst
   * @author Daniel Benkmann (D-III-ITD-D101)
   */
  private static boolean removeConfigThingy(ConfigThingy toRemove, ConfigThingy from)
  {
    Iterator<ConfigThingy> iter = from.iterator();
    while (iter.hasNext())
    {
      ConfigThingy current = iter.next();

      // Falls current das zu löschenden ConfigThingy ist, entfernen wir es und
      // liefern true zurück
      if (toRemove == current)
      {
        iter.remove();
        return true;
      }

      // Falls current nicht das zu löschende ConfigThingy ist, überprüfen wir, ob es
      // Kinder hat und durchsuchen diese rekursiv
      if (current.count() > 0)
      {
        if (removeConfigThingy(toRemove, current))
        {
          // Falls current nach dem rekursiven Entfernen keine Kinder mehr hat,
          // können wir es gleich mitentfernen
          if (current.count() == 0)
          {
            iter.remove();
          }
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Schreibt das übergebene ConfigThingy in die schreibbare Konfigurationsdatei, die
   * sich unter {@link #WRITABLE_CONFIG_PATH} befindet, setzt {@link #writableConfig}
   * auf eine Kopie des übergebenen ConfigThingys, und aktualisiert {@link #config},
   * indem {@link #writableConfig} zu {@link #configWithoutWritable} hinzugefügt
   * wird.
   * 
   * @param newWritableConfig
   *          der neue Inhalt der schreibbaren Konfigurationsdatei
   * 
   * @throws FileNotFoundException
   *           wenn die schreibbare Konfigurationsdatei aus irgendeinem Grund nicht
   *           gefunden (und auch nicht erzeugt) oder nicht geschrieben werden
   *           konnte. Sollte dieser Fehler auftreten macht die Methode keine
   *           Änderungen an {@link #config} oder {@link #writableConfig}.
   * @author Daniel Benkmann (D-III-ITD-D101)
   */
  private void updateWritableConfig(ConfigThingy newWritableConfig)
      throws FileNotFoundException
  {
    /* Schreiben der Änderungen in die schreibbare Konfigurationsdatei */
    File writableConfFile = new File(WRITABLE_CONFIG_PATH);
    PrintStream fileOut = null;
    try
    {
      // Folgende Zeile wirft potentiell eine FileNotFoundException, die wir einfach
      // nach oben weitergeben (daher kein Catch-Block)
      fileOut = new PrintStream(new FileOutputStream(writableConfFile));
      fileOut.print(newWritableConfig.stringRepresentation(true, '"', false));

      // Aktualisieren von this.writableConfig und this.config (hierzu kommen wir
      // nur, wenn es oben keine FileNotFoundException gab
      this.writableConfig = new ConfigThingy(newWritableConfig);
      ConfigThingy newConfig = new ConfigThingy(configWithoutWritable);
      for (ConfigThingy child : this.writableConfig)
      {
        newConfig.addChild(child);
      }
      this.config = newConfig;
    }
    finally
    {
      if (fileOut != null)
      {
        fileOut.close(); // flush & close
      }
    }
  }

  /**
   * Methode zum Testen dieser Klasse.
   * 
   * @param args
   *          wird nicht ausgewertet
   * @author Daniel Benkmann (D-III-ITD-D101)
   */
  public static void main(String[] args)
  {
    SimpleConfigSingleton.initialize();
    SimpleConfigSingleton instance = SimpleConfigSingleton.getInstance();
    try
    {
      System.out.println("Alle Pfade zu Blättern:");
      String[] pathsToLeaves = instance.getPathsToLeaves("");
      for (String p : pathsToLeaves)
      {
        System.out.println(p);
      }
      System.out.println("\n");

      boolean result = false;
      try
      {
        instance.getLeafOrSubtreeByPath("MAK0817/PFAD");
        result = true;
      }
      catch (NoSuchElementException e)
      {
        result = false;
      }
      System.out.println("Ist \"MAK0817/PFAD\" in Config enthalten: " + result);

      try
      {
        instance.getLeafOrSubtreeByPath("MAKROS/MAK0817/PFAD");
        result = true;
      }
      catch (NoSuchElementException e)
      {
        result = false;
      }
      System.out.println("Ist \"MAKROS/MAK0817/PFAD\" in Config enthalten: "
        + result);

      System.out.println("\n\nEin paar Beispielanfragen, die funktionieren sollten:");

      System.out.println("MAKROS/MAK0815/DIALOG1 hat Wert: "
        + instance.getLeafOrSubtreeByPath("MAKROS/MAK0815/DIALOG1"));
      System.out.println("TEST/TEST/TEST/TEST/TEST hat Wert: "
        + instance.getLeafOrSubtreeByPath("TEST/TEST/TEST/TEST/TEST"));
      // Slash am Ende ist optional:
      System.out.println("MAKROS/MAK0815/PFAD/ hat Wert: "
        + instance.getLeafOrSubtreeByPath("MAKROS/MAK0815/PFAD/"));

      System.out.println("\nGibt es überhaupt Blätter: " + instance.hasLeaves());
      System.out.println("\n");

      String path = "MAKROS/MAK0815/DIALOG2";
      String value = "Mein überschriebener Wert\r\nmit Zeilenumbruch";
      System.out.println("Setzen von \"" + path + "\" auf Wert \"" + value
        + "\" und anschließendes Auslesen:");
      result = instance.setLeaf(path, value);
      System.out.println("Ergebnis des Aufrufs von setLeaf: " + result);
      System.out.println("Der Wert von \"" + path + "\" ist: "
        + instance.getLeafOrSubtreeByPath(path));

      System.out.println("\n");
      path = "MAKROS/MAK0815/DIALOG3";
      value = "Dieser String wird gleich gelöscht.";
      System.out.println("Setzen von \"" + path + "\" auf Wert \"" + value);
      result = instance.setLeaf(path, value);
      System.out.println("Ergebnis des Aufrufs von setLeaf: " + result);
      System.out.println("Der Wert von \"" + path + "\" ist: "
        + instance.getLeafOrSubtreeByPath(path));
      System.out.println("Jetzt löschen wir \"" + path + "\" wieder.");
      result = instance.removeNode(path);
      System.out.println("Ergebnis des Aufrufs von removeLeaf: " + result);
      try
      {
        instance.getLeafOrSubtreeByPath(path);
        result = true;
      }
      catch (NoSuchElementException e)
      {
        result = false;
      }
      System.out.println("Ist \"" + path + "\" in Config enthalten: " + result);

      System.out.println("\n");

      String prefix = "MAKROS/";
      System.out.println("Alle Pfade zu Blättern (mit Präfix " + prefix + "):");
      pathsToLeaves = instance.getPathsToLeaves(prefix);
      for (String p : pathsToLeaves)
      {
        System.out.println(p);
      }
      System.out.println("\n");

      SimpleConfig prefixConfig =
        (SimpleConfig) instance.getLeafOrSubtreeByPath(prefix);
      result = false;
      try
      {
        prefixConfig.getByName("MAK0817/PFAD");
        result = true;
      }
      catch (NoSuchElementException e)
      {
        result = false;
      }
      System.out.println("Ist \"MAK0817/PFAD\" in prefixConfig enthalten: " + result);

    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
}
