package de.muenchen.allg.itd51.config.cli;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;

import de.muenchen.allg.itd51.config.parser.ConfigThingy;
import de.muenchen.allg.itd51.config.parser.NodeNotFoundException;
import de.muenchen.allg.itd51.config.parser.SyntaxErrorException;

/**
 * Parse the list of arguments and add them to a configuration file in the
 * syntax of ConfigThingys.
 *
 * @author daniel.sikeler
 *
 */
public class Cli
{
  /**
   * Location of the configuration file.
   */
  private static final File location_conf = new File(
      System.getProperty("user.home") + "/.makro.conf");

  /**
   * Parse the args and extract the key-value-pairs. If a key is defined more
   * than once, the last definition wins.
   *
   * @param args
   *          A list of key-value-pairs separated by '='. A key must contain two
   *          parts separated by '.', eg abc.def=hij.
   * @return A mapping of the keys to the according values.
   * @throws SyntaxErrorException
   *           The args are malformed.
   */
  public static HashMap<String, HashMap<String, String>> parse_args(
      final String[] args) throws SyntaxErrorException
  {
    final HashMap<String, HashMap<String, String>> settings = new HashMap<String, HashMap<String, String>>();
    for (final String arg : args)
    {
      // Split a key-value-pair.
      String[] parts = arg.split("=", 2);
      if (parts.length < 2)
      {
	throw new SyntaxErrorException("Bad setting: " + arg);
      }
      final String param = parts[0];
      final String value = parts[1];
      // Split the key in its two parts.
      parts = param.split("\\.", 2);
      if (parts.length < 2)
      {
	throw new SyntaxErrorException("Bad setting: " + arg);
      }
      final String section = parts[0];
      final String base_param = parts[1];
      // Add the key-value-pair to the map.
      if (!settings.containsKey(section))
      {
	settings.put(section, new HashMap<String, String>());
      }
      final HashMap<String, String> section_settings = settings.get(section);
      section_settings.put(base_param, value);
    }
    return settings;
  }

  /**
   * Add the new configuration properties to the configuration file.
   *
   * @param args
   *          The configuration properties as in {@link #parse_args(String[])}.
   * @throws SyntaxErrorException
   *           The configuration properties are malformed.
   * @throws IOException
   *           The configuration can't be read or written.
   */
  public static void main(final String[] args) throws SyntaxErrorException,
      IOException
  {
    final HashMap<String, HashMap<String, String>> settings = parse_args(args);
    ConfigThingy root;

    if (!location_conf.exists())
    {
      root = new ConfigThingy("conf");
    } else
    {
      root = new ConfigThingy("conf", location_conf.toURI().toURL());
    }

    for (final String section : settings.keySet())
    {
      final HashMap<String, String> section_settings = settings.get(section);

      // This will cause problems if there are more than one ConfigThingy with
      // name section in the configuration-tree.
      ConfigThingy sectionNode;
      try
      {
	sectionNode = root.get(section, 1);
      } catch (final NodeNotFoundException e)
      {
	sectionNode = root.add(section);
      }

      for (final String base_param : section_settings.keySet())
      {
	final String value = section_settings.get(base_param);

	// Same problem as before.
	ConfigThingy node;
	try
	{
	  node = sectionNode.get(base_param, 1);
	} catch (final NodeNotFoundException e)
	{
	  node = sectionNode.add(base_param);
	}

	// Update the value or add it to the configuration.
	try
	{
	  node.getFirstChild().setName(value);
	} catch (final NodeNotFoundException e)
	{
	  node.add(value);
	}
      }
    }

    // Print the configuration to a file.
    try (final PrintStream writer = new PrintStream(new FileOutputStream(
	location_conf)))
    {
      writer.print(root.stringRepresentation(true, '"', false));
    }

  }
}
