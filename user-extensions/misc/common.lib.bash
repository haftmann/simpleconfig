
## configuration and activation of a managed office user extension

HERE="$1"
REGISTRY='/etc/office-user-extensions/extensions.d'
GEAR='/run/office-user-extensions'
JAR_SETTINGS="${HERE}/settings.jar"


function message {
  echo "$1" >&2
}

function fail {
  message "$1"
  exit 1
}

function forall_offices {
  local RAW_NAME="$1"; shift
  [[ ! "${RAW_NAME}" =~ / ]] || fail "Bad extension name: ${RAW_NAME}"

  local EXTENSION_NAME="${RAW_NAME#_}"
    # concession to allow test applications with leading '_'

  local FILE LOC_USER_EXTENSIONS LOC_EXTENSIONS LOC_GEAR
  for ENTRY in "${REGISTRY}"/*
  do
    LOC_USER_EXTENSIONS="$(readlink -f "${ENTRY}")"
    LOC_EXTENSIONS="$(dirname "${LOC_USER_EXTENSIONS}")/share/extensions"
    LOC_GEAR="${GEAR}/$(basename "${ENTRY}")"
    if [[ -e "${LOC_USER_EXTENSIONS}/${EXTENSION_NAME}" ]]
    then
      "$@" "${LOC_USER_EXTENSIONS}/${EXTENSION_NAME}" \
        "${LOC_EXTENSIONS}" "${LOC_GEAR}" "${EXTENSION_NAME}"
    fi
  done
}

function configure {
  local RAW_NAME="$1"

  function configure_single {
    local LOC_USER_EXTENSION="$1"
    local LOC_EXTENSIONS="$2"

    # determine valid parameters
    local LOC_PARAMS="${LOC_USER_EXTENSION}/config/parameters.list"
    if [[ ! -e "${LOC_PARAMS}" ]]
    then
      return
    fi

    # distill settings from valid parameters and environment
    local PARAMS; mapfile -t PARAMS < "${LOC_PARAMS}"
    local SETTINGS=()
    local PARAM; local BASE_PARAM; local VALUE; for PARAM in "${PARAMS[@]}"
    do
      BASE_PARAM="${PARAM##*.}"
      [[ "${!BASE_PARAM+true}" ]] || continue
      SETTINGS+=("${PARAM}=$(expand_user "${!BASE_PARAM}")")
    done
    if (( ${#SETTINGS[@]} == 0 ))
    then
      return
    fi
    java -jar "${JAR_SETTINGS}" "${SETTINGS[@]}"
  }
  forall_offices "${RAW_NAME}" configure_single
}

function link {
  local GROUP_ID="$1"
  local RAW_NAME="$2"

  if [[ -z "${GROUP_ID}" ]]
  then
    fail 'No group id given'
  fi

  local GROUP_NAME="$(getent group "${GROUP_ID}" | cut -d: -f1)"

  if [[ -z "${GROUP_NAME}" ]]
  then
    fail "Bad group id: ${GROUP_ID}"
  fi

  function link_single {
    local LOC_USER_EXTENSION="$1"
    local LOC_EXTENSIONS="$2"
    local GEAR="$3"
    local EXTENSION_NAME="$4"

    # check whether extension is already linked -- purge dead links as side effect
    local LOC_EXT; for LOC_EXT in "${LOC_EXTENSIONS}"/*
    do
      if [[ ! -e "${LOC_EXT}" ]]
      then
        rm "${LOC_EXT}"
      elif [[ "$(readlink -f ${LOC_EXT})" == "$(readlink -f ${LOC_USER_EXTENSION})" ]]
      then
        message "Extension ${EXTENSION_NAME} already linked -- nothing left to do"
        return
      fi
    done

    # link with appropriate access rights
    mkdir -p "${GEAR}/${GROUP_NAME}"
    chown root:"${GROUP_NAME}" "${GEAR}/${GROUP_NAME}"
    chmod 0550 "${GEAR}/${GROUP_NAME}"
    ln -sf "${LOC_USER_EXTENSION}" "${GEAR}/${GROUP_NAME}/${EXTENSION_NAME}"

    # determine unique identifier
    local BASE_NAME="${GROUP_NAME}@${EXTENSION_NAME}"
    local NAME
    local -i SUFFIX; SUFFIX=0
    while true
    do
      NAME="${BASE_NAME}-${SUFFIX}"
      if [[ ! -e "${LOC_EXTENSIONS}/${NAME}" ]]
      then
        break
      fi
      SUFFIX+=1
    done
    ln -s "${GEAR}/${GROUP_NAME}/${EXTENSION_NAME}" "${LOC_EXTENSIONS}/${NAME}"
    chmod 0755 "${LOC_EXTENSIONS}/${NAME}"
    message "Successfully linked extension ${EXTENSION_NAME}"
      ## safe under the assumption that the user is anytime free to
      ## install and remove extensions
  }
  forall_offices "${RAW_NAME}" link_single
}
